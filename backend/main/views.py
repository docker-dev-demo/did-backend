from .auxlib import generate_secret, generate_totp
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import QRCode


class GenerateQRCode(APIView):
    """This view generates a new QR code and marks all the others as inactive."""

    def get(self, request):
        QRCode.objects.filter(is_active=True).update(is_active=False)
        qr = QRCode.objects.create(secret=generate_secret())
        return Response({"qrcode": qr.image})


class CheckCode(APIView):
    """This view checks that a given TOTP is valid."""

    def post(self, request):
        qr = QRCode.objects.get(is_active=True)
        possible_totp = request.data["code"]
        totp = str(generate_totp(qr.secret)).zfill(6)
        return Response({"accepted": possible_totp == totp})
