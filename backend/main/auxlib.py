def generate_secret(secret_len=8):
    from secrets import token_hex as th

    return th(secret_len)


def to_b32(string):
    from base64 import b32encode as b32

    return b32(string.encode()).decode().replace("=", "")


def dynamic_truncation(hs, length):
    """This algorithm is described in RFC 4226 and the code (in python) for this
    algorithm was inspired by the source code of django-otp (oath.hotp) and the
    pseudocode presented in RFC 4226.
    """

    offset = hs[19] & 0x0F
    bin_code = (
        (hs[offset] & 0x7F) << 24
        | (hs[offset + 1] & 0xFF) << 16
        | (hs[offset + 2] & 0xFF) << 8
        | (hs[offset + 3] & 0xFF)
    )
    return bin_code % 10 ** length


def generate_totp(k, l=6, period=30, algorithm="sha1"):
    """The algorithm is described in RFC 6238 and the code (in python) is heavily
    inspired by this article:
    https://hackernoon.com/implementing-2fa-how-time-based-one-time-password-actually-works-with-python-examples-cm1m3ywt
    """

    from math import floor
    from time import time
    from hmac import new as hnew
    import hashlib

    hash_fn = getattr(hashlib, algorithm.lower())

    now_in_seconds = floor(time())
    t = floor(now_in_seconds / period)
    h = hnew(
        bytes(k, encoding="utf-8"), t.to_bytes(length=8, byteorder="big"), hash_fn
    ).digest()

    dt = dynamic_truncation(h, l)
    return dt
