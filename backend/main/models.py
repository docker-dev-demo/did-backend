from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.db import models


def fk(to, **kwargs):
    return models.ForeignKey(to, on_delete=models.CASCADE, **kwargs)


def get_file_directory(instance, filename, subdir):
    return f"{subdir}/{filename}"


def get_qrcode_file_directory(instance, filename):
    return get_file_directory(instance, filename, "qrcodes")


class Base(models.Model):
    id = models.AutoField(primary_key=True, editable=False)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def delete(self, *args, **kwargs):
        """Set default delete behavior to mark is_active false."""

        self.is_active = False
        self.save()

    def hard_delete(self, *args, **kwargs):
        """Add method for completely deleting an object."""

        super().delete(*args, **kwargs)

    class Meta:
        abstract = True


class User(AbstractUser, Base):
    pass

    class Meta:
        db_table = "user"

    def save(self, *args, **kwargs):
        if not self.password.startswith("pbkdf2_sha256$"):
            from django.contrib.auth.hashers import make_password

            self.password = make_password(self.password)
        super().save(*args, **kwargs)


class QRCode(Base):
    algorithm = models.CharField(default="SHA1", max_length=16)
    digits = models.PositiveIntegerField(default=6)
    issuer = models.CharField(default="VISIMO", max_length=32)
    label = models.CharField(default="DiD Authenticator", max_length=32)
    period = models.PositiveIntegerField(default=30)
    secret = models.CharField(max_length=32)
    type = models.CharField(default="totp", max_length=10)
    image = models.FileField(upload_to=get_qrcode_file_directory, blank=True, null=True)

    class Meta:
        db_table = "qrcode"

    @property
    def string_data(self):
        """Note that when we generate the string data for the QR code, we encode the
        secret as base32. This is used EXCLUSIVELY for transport to the authenticator
        device and when the TOTP is generated, it should be generated from the root
        secret."""

        from urllib.parse import quote
        from main.auxlib import to_b32

        return "otpauth://" + (
            f"{quote(self.type)}/{quote(self.label)}?secret={quote(to_b32(self.secret))}"
            f"&issuer={quote(self.issuer)}&algorithm={quote(self.algorithm)}"
            f"&digits={self.digits}&period={self.period}"
        )

    def save(self, *args, **kwargs):
        from pyqrcode import create
        from django.core.files.images import ImageFile
        from io import BytesIO

        svg = create(self.string_data)
        strbuf = BytesIO()
        svg.svg(strbuf, scale=4)
        # GIVE IMAGE A NAME
        self.image = ImageFile(strbuf, name=f"qr{self.secret[:6]}.svg")
        super().save(*args, **kwargs)
