# build stage
FROM python:3.9.4-slim as builder

WORKDIR /app

COPY requirements.txt .
RUN pip install --user --no-warn-script-location -r requirements.txt


# app stage
FROM python:3.9.4-slim as app

WORKDIR /app

COPY --from=builder /root/.local /root/.local

ENV PYTHONUNBUFFERED 1
ENV PATH=/root/.local/bin:$PATH

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
